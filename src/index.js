import {canvas, context} from './canvas'
import Player from './class/Player'
import { CANVAS_SCALE } from './constants'
import store, { background } from './data'
import { floorCollisions, platformCollisions } from './data/collisions'
import { handleGenerateCollisionBlock, scaledCanvas } from './utils'


//generate collistionBlock
const floorCollisionBlocks = handleGenerateCollisionBlock(floorCollisions)
const platformCollisionBlocks = handleGenerateCollisionBlock(platformCollisions, 4)

//classes
const player = new Player({
    position:{
        x:100,
        y:300
    },
    imageSrc:'./assets/warrior/Idle.png',
    collisionBlocks:floorCollisionBlocks,
    platformCollisionBlocks,
    frameRate:8,
    scale:0.5,
    animations:{
        Idle:{
            imageSrc:'./assets/warrior/Idle.png',
            frameRate:8,
            frameBuffer:3
        },
        Run:{
            imageSrc:'./assets/warrior/Run.png',
            frameRate:8,
            frameBuffer:5
        },
        RunLeft:{
            imageSrc:'./assets/warrior/RunLeft.png',
            frameRate:8,
            frameBuffer:5
        },
        Jump:{
            imageSrc:'./assets/warrior/Jump.png',
            frameRate:2,
            frameBuffer:4
        },
        JumpLeft:{
            imageSrc:'./assets/warrior/JumpLeft.png',
            frameRate:2,
            frameBuffer:4
        },
        Fall:{
            imageSrc:'./assets/warrior/Fall.png',
            frameRate:2,
            frameBuffer:4
        },
        FallLeft:{
            imageSrc:'./assets/warrior/FallLeft.png',
            frameRate:2,
            frameBuffer:4
        },
    }
})






function animate(){
    window.requestAnimationFrame(animate)
    context.fillStyle = 'white'
    context.fillRect(0,0,canvas.width,canvas.height)

    // only scale the background
    context.save()
    context.scale(CANVAS_SCALE, CANVAS_SCALE)
    context.translate(store.camera.position.x,store.camera.position.y)
    background.update()
    
    // floorCollisionBlocks.forEach(collisionBlock=>collisionBlock.update())
    // platformCollisionBlocks.forEach(collisionBlock => collisionBlock.update())

    player.checkForHorizontalCanvasCollisions()
    player.update()
    context.restore()

    player.velocity.x = 0 
    if(store.keys.d.pressed) {
        player.switchSprite('Run')
        player.velocity.x = 2
        player.shouldPanCameraToTheLeft({canvas, camera:store.camera})
        
    }
    else if (store.keys.a.pressed) {
        player.switchSprite('RunLeft')
        player.velocity.x = -2
        player.shouldPanCameraToTheRight({canvas, camera:store.camera})
    }
    else if (player.velocity.y === 0) player.switchSprite('Idle')

    if( player.velocity.y < 0 && player.velocity.x >= 0){
        player.shouldPanCameraDown({camera:store.camera, canvas})
        player.switchSprite('Jump')
    }
    else if( player.velocity.y < 0 && player.velocity.x < 0){
        player.shouldPanCameraDown({camera:store.camera, canvas})
        player.switchSprite('JumpLeft')
    }
    else if (player.velocity.y > 0 && player.velocity.x >= 0) {
        player.shouldPanCameraUp({camera:store.camera, canvas})
        player.switchSprite('Fall')
    }
    else if (player.velocity.y > 0 && player.velocity.x < 0) {
        player.shouldPanCameraUp({camera:store.camera, canvas})
        player.switchSprite('FallLeft')
    }
}

animate()

//event listener
window.addEventListener('keydown',(e)=>{
    switch (e.key){
        case 'd':
            store.updateKeys('d',true)
        break
        case 'a':
            store.updateKeys('a',true)
        break
        case 'w':
            player.velocity.y = -4
        break
    }
})

window.addEventListener('keyup',(e)=>{
    switch (e.key){
        case 'd':
            store.updateKeys('d',false)
        break
        case 'a':
            store.updateKeys('a',false)
        break
    }
})