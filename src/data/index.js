import Sprite from "../class/Sprite";
import Store from "../class/Store";
import { scaledCanvas } from "../utils";

export const background = new Sprite({
    position:{
        x:0,
        y:0
    },
    imageSrc:"./assets/background.png"
})


const backgroundImageHeight = 432
const store = new Store({
    keys:{
        a:{
            pressed:false
        },
        w:{
            pressed:false
        },
        s:{
            pressed:false
        },
        d:{
            pressed:false
        }
    },
    camera:{
        position:{
            x:0,
            y:-backgroundImageHeight + scaledCanvas.height
        }
    }
})

export default store