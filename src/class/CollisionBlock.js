import { context } from "../canvas"

class CollisionBlock{
    constructor({position, height = 16}){
        this.position= position
        this.width = 16
        this.height = height
    }

    draw(){
         context.fillStyle = 'rgba(255,0,0,0.3)'
        context.fillRect(this.position.x, this.position.y, this.width, this.height)
    }

    update(){
        this.draw()
    }
}

export default CollisionBlock