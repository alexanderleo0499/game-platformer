class Store{
    constructor({keys, camera}){
        this.keys = keys
        this.camera = camera
    }
    updateKeys(key, value){
        this.keys[key].pressed = value
    }

    updateCamera(key, value){
        this.camera[key] = value
    }

}

export default Store