export const canvas = document.getElementById('canvas')

export const context = canvas.getContext('2d')

//settings
canvas.width = window.innerWidth
canvas.height = window.innerHeight
