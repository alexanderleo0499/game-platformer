import { canvas } from "../canvas"
import CollisionBlock from "../class/CollisionBlock"

export const scaledCanvas = {
    width:canvas.width / 4,
    height:canvas.height / 4
}

export const handleGenerateCollisionBlock = (collisionData, height=16) => {
    const collisions2D = []
    for(let i=0; i< collisionData.length; i+=36){
        collisions2D.push(collisionData.slice(i, i+36))
    }

    const collisionBlocks = []
    collisions2D.forEach((row, y)=>{
        row.forEach((symbol,x)=>{
            if(symbol === 202){
                collisionBlocks.push(new CollisionBlock({
                    position:{
                        x:x*16,
                        y:y*16
                    },
                    height
                }))
            }
        })
    })

    return collisionBlocks
}

export const collision = ({player, collisionBlock}) => {
    return (
        player.position.y + player.height >= collisionBlock.position.y && //the bottom
        player.position.y <= collisionBlock.position.y + collisionBlock.height && //the top
        player.position.x <= collisionBlock.position.x + collisionBlock.width && //the left
        player.position.x + player.width >= collisionBlock.position.x // the right
    )
}

export const platformCollision = ({player, collisionBlock}) => {
    return (
        player.position.y + player.height >= collisionBlock.position.y && //the bottom
        player.position.y + player.height <= collisionBlock.position.y + collisionBlock.height && //the top
        player.position.x <= collisionBlock.position.x + collisionBlock.width && //the left
        player.position.x + player.width >= collisionBlock.position.x // the right
    )
}